var path = require("path");

var webpack = require("webpack");

var config = {
	entry: "./src/app/app.jsx",
	output: {
		path: path.resolve("build"),
		filename: "bundle.js"
	},
	resolve: {
		modulesDirectories: ["node_modules", "src"],
		root: path.resolve("./src"),
		extensions: ["", ".js"]
	},
	module: {
		preLoaders: [],
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: "babel",
				query: {
					presets: ["react", "es2015"]
				}
			},
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				loaders: ["style", "css", "sass"]
			},
			{
				test: /\.(png|jpg)$/,
				loader: "url-loader?limit=8192"
			}
		]
	},
	devtool: "source-map",
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			output: {
				comments: false
			},
			compress: {
				warnings: false,
				screw_ie8: true
			}
		})
	]
};

module.exports = config;