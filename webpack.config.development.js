var path = require("path");

var webpack = require("webpack");

module.exports = {
	devtool: "source-map",
	devServer: {
		contentBase: path.resolve("build"),
		historyApiFallback: true
	},
	entry: [
		"webpack-dev-server/client?http://127.0.0.1:8080",
		"webpack/hot/only-dev-server",
		"./src/app/app.jsx"
	],
	output: {
		path: path.resolve("build"),
		filename: "bundle.js"
	},
	resolve: {
		modulesDirectories: ["node_modules", "src"],
		root: path.resolve("src"),
		extensions: ["", ".js"]
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /[\\\/]node_modules[\\\/](?!react-redux-grid)/,
				loaders: ["react-hot", "babel?presets[]=react,presets[]=es2015,presets[]=stage-0"]
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				loaders: ["style", "css"]
			},
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				loaders: ["style", "css", "sass"]
			},
			{
				test: /\.(png|jpg)$/,
				exclude: /node_modules/,
				loader: "url-loader?limit=8192"
			},
			{
				test: /\.styl$/,
				exclude: /[\\\/]node_modules[\\\/](?!react-redux-grid)/,
				loaders: ["style-loader", "css-loader", "stylus-loader"]
			},
			{
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				exclude: /[\\\/]node_modules[\\\/](?!react-redux-grid)/,
				loaders: ["url-loader?limit=10000&mimetype=application/font-woff" ]
			},
			{
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				exclude: /[\\\/]node_modules[\\\/](?!react-redux-grid)/,
				loaders: ["file-loader"]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	]
};