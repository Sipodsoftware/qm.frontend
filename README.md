# QM - frontend

### Requirements

- Python
- Visual Studio 2012
- Node.js
- Install webpack and webpack-dev-server globally `npm install -g webpack webpack-dev-server`

### Stuff to do when you get the repo first time

Once you download whole repository open up a terminal and position yourself to that project.

Execute:

- Install npm dependencies `npm install`