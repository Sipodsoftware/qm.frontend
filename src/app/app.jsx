import React from "react";
import ReactDOM from "react-dom";
import injectTapEventPlugin from "react-tap-event-plugin";

import store from "./store";
import App from "./components/app/app.jsx";

//TODO: Remove when react release first official version of React
// Needed for onTouchTap
injectTapEventPlugin();

ReactDOM.render(<App store={store} />, document.getElementById("app"));