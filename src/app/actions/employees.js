import axios from "axios";
import {Store} from "react-redux-grid";
import {setData} from "react-redux-grid/src/actions/GridActions";
// ACTION TYPES

export const RECEIVED_EMPLOYEES = "RECEIVED_EMPLOYEES";
export const RECEIVED_EMPLOYEE = "RECEIVED_EMPLOYEE";

// ACTION CREATORS

export function getEmployees() {
	return function (dispatch) {
		return axios.get("http://localhost:3000/employees").then(function (res) {
			return dispatch(receivedEmployeesSetGridData(res.data));
		});
	}
}

export function getEmployee(id) {
	return function (dispatch) {
		return axios.get(`http://localhost:3000/employees/${id}`).then(function (res) {
			return dispatch(receivedEmployee(res.data));
		});
	}
}

export function receivedEmployeesSetGridData(employees) {
	return function (dispatch) {
		dispatch(receivedEmployees(employees));
		Store.dispatch(setData(employees));
	}
}

export function receivedEmployees(employees) {
	return {
		type: RECEIVED_EMPLOYEES,
		payload: {
			employees
		}
	}
}

export function receivedEmployee(currentEmployee) {
	return {
		type: RECEIVED_EMPLOYEE,
		payload: {
			currentEmployee
		}
	}
}