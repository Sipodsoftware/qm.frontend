// ACTION TYPES

export const OPEN_DRAWER = "OPEN_DRAWER";
export const CLOSE_DRAWER = "CLOSE_DRAWER";
export const SET_DRAWER_VISIBILITY = "SET_DRAWER_VISIBILITY";

// ACTION CREATORS

export function openDrawer() {
	return {
		type: OPEN_DRAWER,
		payload: {
			open: true
		}
	};
}

export function closeDrawer() {
	return {
		type: CLOSE_DRAWER,
		payload: {
			open: false
		}
	}
}

export function setDrawerVisibility(open) {
	return {
		type: SET_DRAWER_VISIBILITY,
		payload: {
			open: open
		}
	}
}