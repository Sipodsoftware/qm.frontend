import React from "react";
import {connect} from "react-redux";

// Components
import LocationsMap from "./locations.map.jsx";

const LocationsMapContainer = React.createClass({
	render() {
		return (
			<LocationsMap />
		);
	}
});

function mapStateToProps(state) {
	return {};
}

export default connect(mapStateToProps)(LocationsMapContainer);