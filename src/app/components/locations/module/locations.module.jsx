import React from "react";
import {WidthProvider} from "react-grid-layout";

// Setting grid to be responsive
var ResponsiveReactGridLayout = WidthProvider(require("react-grid-layout").Responsive);

// Layout configuration
import {LOCATION_MAP, layout} from "./layout.config";

// Components
import LocationsMapContainer from "../map/locations.map.container.jsx";

const LocationsModule = React.createClass({
	render() {
		return (
			<ResponsiveReactGridLayout
				{...layout}
			>
				<div key={LOCATION_MAP}>
					<LocationsMapContainer />
				</div>
			</ResponsiveReactGridLayout>
		);
	}
});

export default LocationsModule;
