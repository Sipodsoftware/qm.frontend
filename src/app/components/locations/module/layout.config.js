export const LOCATION_MAP = "LOCATION_MAP";

export const layout = {
	rowHeight: 30,
	cols: {lg: 12, md: 10, sm: 6, xs: 4, xxs: 2},
	layouts: {
		lg: [
			{
				i: LOCATION_MAP,
				x: 4,
				y: 0,
				w: 4,
				h: 12,
				minH: 4,
				minW: 2
			}
		]
	}	
};