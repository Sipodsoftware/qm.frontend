import React from "react";
import AppBar from "material-ui/lib/app-bar";

const Header = React.createClass({
	render() {
		const {openDrawerHandler} = this.props;
		return (
			<AppBar
				title="Quality management"
				iconClassNameRight="muidocs-icon-navigation-expand-more"
				onLeftIconButtonTouchTap={openDrawerHandler}
			/>
		);
	}
});

export default Header;