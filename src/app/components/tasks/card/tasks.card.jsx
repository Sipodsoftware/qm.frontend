import React from "react";
import Card from "../../../../../node_modules/material-ui/lib/card/card";
import CardActions from "../../../../../node_modules/material-ui/lib/card/card-actions";
import CardHeader from "../../../../../node_modules/material-ui/lib/card/card-header";
import RaisedButton from "../../../../../node_modules/material-ui/lib/raised-button";
import CardText from "../../../../../node_modules/material-ui/lib/card/card-text";

const TasksCard = React.createClass({

	render() {
		return (
			<Card>
				<CardHeader
					title="Tasks"
				/>
				<CardText>
					Show tasks in a list. <br/>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
					Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
					Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
				</CardText>
				<CardActions>
					<RaisedButton primary={true} label="Show tasks" />
				</CardActions>
			</Card>
		);
	}
});

export default TasksCard;