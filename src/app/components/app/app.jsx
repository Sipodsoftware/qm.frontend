import React from "react";
import {Router, Route, browserHistory} from "react-router";
import {Provider} from "react-redux";
import MuiThemeProvider from "material-ui/lib/MuiThemeProvider";
import getMuiTheme from "material-ui/lib/styles/getMuiTheme";

// Vendor styles
require("!style!css!react-grid-layout/css/styles.css");
require("!style!css!react-resizable/css/styles.css");

// Styles
require("../../assets/style/app.scss");
require("../../assets/style/material.ui.overrides.scss");
require("../../assets/style/react.redux.grid.overrides.scss");

// MUI default theme
import defaultMuiTheme from "../../assets/themes/default.mui.theme";

// Layouts
import AppLayout from "../../layouts/app/app.jsx";

// Modules
import EmployeesModule from "../employees/module/employees.module.jsx";
import LocationsModule from "../locations/module/locations.module.jsx";
import HomeModule from "../home/module/home.module.jsx";

const muiTheme = getMuiTheme(defaultMuiTheme);

export default React.createClass({
	propTypes: {
		store: React.PropTypes.object.isRequired
	},
	render() {
		const {store} = this.props;
		return (
			<MuiThemeProvider muiTheme={muiTheme}>
				<Provider store={store}>
					<Router history={browserHistory}>
						<Route component={AppLayout}>
							<Route path="/" component={HomeModule} />
							<Route path="/employees" component={EmployeesModule} />
							<Route path="/locations" component={LocationsModule} />
						</Route>
					</Router>
				</Provider>
			</MuiThemeProvider>
		);
	}
});