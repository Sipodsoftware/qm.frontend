import React from "react";
import {WidthProvider} from "react-grid-layout";

// Setting grid to be responsive
var ResponsiveReactGridLayout = WidthProvider(require("react-grid-layout").Responsive);

// Layout configuration
import {EMPLOYEES_GRID, EMPLOYEE_DETAILS, EMPLOYEES_COUNT_CHART, layout} from "./layout.config";

// Components
import EmployeesGridContainer from "../grid/employees.grid.container.jsx";
import EmployeeDetailsContainer from "../details/employee.details.container.jsx";
import EmployeesCountChartContainer from "../charts/employees-count/employees.count.chart.container.jsx";

const EmployeesContainer = React.createClass({
	render() {
		return (
			<ResponsiveReactGridLayout
				{...layout}
			>
				<div key={EMPLOYEES_GRID}>
					<EmployeesGridContainer />
				</div>
				<div key={EMPLOYEE_DETAILS}>
					<EmployeeDetailsContainer />
				</div>
				<div key={EMPLOYEES_COUNT_CHART}>
					<EmployeesCountChartContainer />
				</div>
			</ResponsiveReactGridLayout>
		);
	}
});

export default EmployeesContainer;
