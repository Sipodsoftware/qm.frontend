export const EMPLOYEES_GRID = "EMPLOYEES_GRID";
export const EMPLOYEE_DETAILS = "EMPLOYEE_DETAILS";
export const EMPLOYEES_COUNT_CHART = "EMPLOYEES_COUNT_CHART";

export const layout = {
	rowHeight: 30,
	cols: {lg: 12, md: 10, sm: 6, xs: 4, xxs: 2},
	draggableHandle: '.subheader',
	layouts: {
		lg: [
			{
				i: EMPLOYEES_GRID,
				x: 0,
				y: 0,
				w: 4,
				h: 12,
				minH: 5,
				minW: 2
			},
			{
				i: EMPLOYEE_DETAILS,
				x: 4,
				y: 0,
				w: 4,
				h: 12,
				minH: 4,
				minW: 2
			},
			{
				i: EMPLOYEES_COUNT_CHART,
				x: 8,
				y: 0,
				w: 4,
				h: 12,
				minH: 6,
				minW: 3
			}
		]
	}	
};