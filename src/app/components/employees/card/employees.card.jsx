import React from "react";
import {Link} from "react-router";
import Card from "material-ui/lib/card/card";
import CardActions from "material-ui/lib/card/card-actions";
import CardHeader from "material-ui/lib/card/card-header";
import RaisedButton from "material-ui/lib/raised-button";
import CardText from "material-ui/lib/card/card-text";

const EmployeesCard = React.createClass({
	render() {
		return (
			<Card>
				<CardHeader
					title="Employees"
				/>
				<CardText>
					Show employees in a list. <br/>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
					Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
					Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
					Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
					Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
					Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
				</CardText>
				<CardActions>
					<Link to="/employees"><RaisedButton primary={true} label="Show employees" /></Link>
				</CardActions>
			</Card>
		);
	}
});

export default EmployeesCard;