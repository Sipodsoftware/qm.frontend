import React from "react";
import {connect} from "react-redux";

// Components
import EmployeesGrid from "./employees.grid.jsx";

// Actions
import {getEmployees, getEmployee} from "../../../actions/employees";

const EmployeeGridContainer = React.createClass({
	componentDidMount() {
		this.props.dispatch(getEmployees());
	},
	handleRowClick(row) {
		this.props.dispatch(getEmployee(row.id));
	},
	render() {
		const {employees} = this.props;

		return (
			<EmployeesGrid
				employees={employees}
				handleRowClick={this.handleRowClick}
			/>
		);
	}
});

function mapStateToProps(state) {
	return {
		employees: state.employeesReducer.employees
	};
}

export default connect(mapStateToProps)(EmployeeGridContainer);