import React from "react";
import Paper from "../../../../../node_modules/material-ui/lib/paper";
import Subheader from "material-ui/lib/Subheader";
import Divider from "../../../../../node_modules/material-ui/lib/divider";
import {Grid, Store} from "react-redux-grid";

const EmployeesGrid = React.createClass({
	thisEmployeesGrid: this,
	gridData: {
		store: Store,
		data: [],
		columns: [
			{
				name: "First Name",
				dataIndex: "firstName",
				width: "50%"
			},
			{
				name: "Last Name",
				dataIndex: "lastName",
				width: "50%"
			}
		],
		plugins: {
			COLUMN_MANAGER: {
				resizable: false,
				moveable: false,
				sortable: {
					enabled: false
				}
			},
			FILTER_CONTAINER: {
				enabled: false
			},
			EDITOR: {
				enabled: false
			},
			PAGER: {
				enabled: false
			},
			LOADER: {
				enabled: true
			},
			SELECTION_MODEL: {
				enabled: true,
				mode: "single",
				allowDeselect: false,
				activeCls: "active",
				selectionEvent: "singleclick",
				editEvent: "none"
			},
			ERROR_HANDLER: {
				defaultErrorMessage: "AN ERROR OCURRED",
				enabled: true
			},
			BULK_ACTIONS: {
				enabled: false
			}
		}
	},
	render() {
		const thisEmployeesGrid = this;
		var events = {
			HANDLE_ROW_CLICK: function (row, reactEvent, id, browserEvent) {
				thisEmployeesGrid.props.handleRowClick(row);
			}
		};

		return (
			<Paper zDepth={2} className="paper">
				<Subheader className="subheader">Employees</Subheader>
				<Divider />
				<Grid {...this.gridData} events={events} />
			</Paper>
		);
	}
});

export default EmployeesGrid;

