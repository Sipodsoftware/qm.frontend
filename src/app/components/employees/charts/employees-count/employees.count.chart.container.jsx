import React from "react";
import {connect} from "react-redux";

//Components
import EmployeesCountChart from "./employees.count.chart.jsx";

const EmployeesCountChartContainer = React.createClass({
	render() {
		const {employeesCountChartData} = this.props;
		return (
			
			<EmployeesCountChart data={employeesCountChartData} />
		);	
	}
});

function mapStateToProps(state) {
	return {
		
	};
}

export default connect(mapStateToProps)(EmployeesCountChartContainer);