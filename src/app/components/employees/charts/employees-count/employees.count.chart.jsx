import React from "react";
import Paper from "material-ui/lib/paper";
import Subheader from "material-ui/lib/Subheader";
import Divider from "material-ui/lib/divider";
import {Bar} from "react-chartjs";

const charData = {
	labels: ["January", "February", "March", "April", "May", "June", "July"],
	datasets: [
		{
			label: "My First dataset",
			fillColor: "rgba(220,220,220,0.5)",
			strokeColor: "rgba(220,220,220,0.8)",
			highlightFill: "rgba(220,220,220,0.75)",
			highlightStroke: "rgba(220,220,220,1)",
			data: [65, 59, 80, 81, 56, 55, 40]
		},
		{
			label: "My Second dataset",
			fillColor: "rgba(151,187,205,0.5)",
			strokeColor: "rgba(151,187,205,0.8)",
			highlightFill: "rgba(151,187,205,0.75)",
			highlightStroke: "rgba(151,187,205,1)",
			data: [28, 48, 40, 19, 86, 27, 90]
		}
	]
};

const EmployeesCountChart = React.createClass({
	render() {
		return (
			<Paper zDepth={2} className="paper">
				<Subheader
					className="subheader">Employee's count 2016</Subheader>
				<Divider />
				<Bar data={charData} width="500" height="300"/>
			</Paper>
		);
	}
});

export default EmployeesCountChart;