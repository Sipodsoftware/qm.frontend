import React from "react";
import {connect} from "react-redux";

// Components
import EmployeeDetails from "./employee.details.jsx";

const EmployeeDetailsContainer = React.createClass({
	render() {
		const {currentEmployee} = this.props;

		return (
			<EmployeeDetails
				currentEmployee={currentEmployee}
			/>
		);
	}
});

function mapStateToProps(state) {
	return {
		currentEmployee: state.employeesReducer.currentEmployee
	};
}

export default connect(mapStateToProps)(EmployeeDetailsContainer);