import React from "react";
import Paper from "material-ui/lib/paper";
import Subheader from "material-ui/lib/Subheader";
import Divider from "material-ui/lib/divider";
import TextField from "material-ui/lib/text-field";

// Styles
require('./employee.details.scss');

const EmployeeDetails = React.createClass({
	render() {
		const {currentEmployee} = this.props;
		var header = "Employee's information";
		var content = (<div className="no-employee-selected-header">Select employee from the list.</div>);
		
		if (currentEmployee) {
			header = `${currentEmployee.firstName} ${currentEmployee.lastName}`;
			content = (
				<div style={{padding: "10px 20px"}}>
					<TextField
						className="employee-details-input-box"
						hintText="Enter user's first name"
						floatingLabelText="First name"
						type="text"
						defaultValue={currentEmployee.firstName}
						key={currentEmployee.firstName}
					/><br />
					<TextField
						className="employee-details-input-box"
						hintText="Enter user's last name"
						floatingLabelText="Last name"
						type="text"
						defaultValue={currentEmployee.lastName}
						key={currentEmployee.lastName}
					/><br />
					<TextField
						className="employee-details-input-box"
						hintText="Enter user's gender"
						floatingLabelText="Gender"
						type="text"
						defaultValue={currentEmployee.gender}
						key={currentEmployee.gender}
					/><br />
					<TextField
						className="employee-details-input-box"
						hintText="Enter user's email"
						floatingLabelText="Email"
						type="email"
						defaultValue={currentEmployee.email}
						key={currentEmployee.email}
					/><br />
					<TextField
						className="employee-details-input-box"
						hintText="Enter user's address"
						floatingLabelText="Address"
						type="text"
						defaultValue={currentEmployee.address}
						key={currentEmployee.address}
					/><br />
				</div>
			);
		}
		return (
			<Paper zDepth={2} className="paper">
				<Subheader
					className="subheader">{header}</Subheader>
				<Divider />
				{content}
			</Paper>
		);
	}
});

export default EmployeeDetails;