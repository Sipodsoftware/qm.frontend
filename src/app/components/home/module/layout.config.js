export const EMPLOYEES_CARD = "EMPLOYEES_CARD";
export const TASKS_CARD = "TASKS_CARD";
export const LOCATIONS_CARD = "LOCATIONS_CARD";

export const layout = {
	rowHeight: 30,
	cols: {lg: 12, md: 10, sm: 6, xs: 4, xxs: 2},
	isDraggable: false,
	isResizable: false,
	layouts: {
		lg: [
			{
				i: EMPLOYEES_CARD,
				x: 0,
				y: 0,
				w: 4,
				h: 12
			},
			{
				i: TASKS_CARD,
				x: 4,
				y: 0,
				w: 4,
				h: 12
			},
			{
				i: LOCATIONS_CARD,
				x: 8,
				y: 0,
				w: 4,
				h: 12
			}
		]
	}
};