import React from "react";
import {WidthProvider} from "react-grid-layout";

// Setting grid to be responsive
var ResponsiveReactGridLayout = WidthProvider(require("react-grid-layout").Responsive);

// Layout configuration
import {EMPLOYEES_CARD, TASKS_CARD, LOCATIONS_CARD, layout} from "./layout.config";

// Components
import EmployeesCard from "../../employees/card/employees.card.jsx";
import TasksCard from "../../tasks/card/tasks.card.jsx";
import LocationsCard from "../../locations/card/locations.card.jsx";

const Home = React.createClass({
	render() {
		return (
			<div>
				<ResponsiveReactGridLayout
					{...layout}
				>
					<div key={EMPLOYEES_CARD}>
						<EmployeesCard />
					</div>
					<div key={TASKS_CARD}>
						<TasksCard />
					</div>
					<div key={LOCATIONS_CARD}>
						<LocationsCard />
					</div>
				</ResponsiveReactGridLayout>
			</div>
		);
	}
});

export default Home;