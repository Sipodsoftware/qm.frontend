import React from "react";
import {connect} from "react-redux";
import {browserHistory} from "react-router"
import LeftNav from "material-ui/lib/left-nav";
import ListItem from "material-ui/lib/lists/list-item";

// Styles
require("./drawer.scss");

// Actions
import {closeDrawer} from "../../actions/common/drawer";

const Drawer = React.createClass({
	changeModule(modulePathName) {
		browserHistory.push(modulePathName);
		this.props.dispatch(closeDrawer());
	},
	render() {
		const {open, onRequestChangeHandler} = this.props;
		return (
			<LeftNav
				docked={false}
				width={300}
				open={open}
				disableSwipeToOpen={false}
				onRequestChange={onRequestChangeHandler}
				swipeAreaWidth={50}
			>
				<ListItem
					className="drawer-item-header"
					primaryText="Modules"
					disabled={true}
				/>
				<ListItem
					primaryText="Employees"
				    onTouchTap={this.changeModule.bind(this, "/employees")}
				/>
				<ListItem
					primaryText="Locations"
				    onTouchTap={this.changeModule.bind(this, "/locations")}
				/>
				<ListItem
					primaryText="Tasks"
				/>
			</LeftNav>
		);
	}
});

function mapStateToProps(state) {
	return {
		open: state.commonDrawerReducer.open
	};
}

export default connect(mapStateToProps)(Drawer);

