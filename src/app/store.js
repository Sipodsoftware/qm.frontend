import {createStore, applyMiddleware, combineReducers} from "redux";
import thunkMiddleware from "redux-thunk";

// Reducers
import commonDrawerReducer from "./reducers/common/drawer"
import employeesReducer from "./reducers/employees";

var reducers = combineReducers({
	commonDrawerReducer: commonDrawerReducer,
	employeesReducer: employeesReducer
});

// exporting store
export default createStore(reducers, applyMiddleware(thunkMiddleware));