import {
	indigo900,
	pinkA200,
	grey100, grey300, grey400, grey500,
	white, darkBlack, fullBlack,
} from 'material-ui/lib/styles/colors';
import ColorManipulator from 'material-ui/lib/utils/color-manipulator';

const spacing = {
	iconSize: 24,

	desktopGutter: 24,
	desktopGutterMore: 32,
	desktopGutterLess: 16,
	desktopGutterMini: 8,
	desktopKeylineIncrement: 64,
	desktopDropDownMenuItemHeight: 32,
	desktopDropDownMenuFontSize: 15,
	desktopDrawerMenuItemHeight: 48,
	desktopSubheaderHeight: 48,
	desktopToolbarHeight: 56
};

const palette = {
	primary1Color: indigo900,
	primary2Color: indigo900,
	primary3Color: grey400,
	accent1Color: pinkA200,
	accent2Color: grey100,
	accent3Color: grey500,
	textColor: darkBlack,
	alternateTextColor: white,
	canvasColor: white,
	borderColor: grey300,
	disabledColor: ColorManipulator.fade(darkBlack, 0.3),
	pickerHeaderColor: indigo900,
	clockCircleColor: ColorManipulator.fade(darkBlack, 0.07),
	shadowColor: fullBlack
};

export default {
	spacing: spacing,
	fontFamily: 'Roboto, sans-serif',
	palette: palette
};

