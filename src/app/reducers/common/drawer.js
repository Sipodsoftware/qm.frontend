import {OPEN_DRAWER, CLOSE_DRAWER, SET_DRAWER_VISIBILITY} from "../../actions/common/drawer";

const initialState = {
	open: false
};

function commonDrawerReducer(state = initialState, action) {
	switch (action.type) {
		case OPEN_DRAWER: {
			return drawerOpenPropReducer(state, action);
		}
		case CLOSE_DRAWER: {
			return drawerOpenPropReducer(state, action);
		}
		case SET_DRAWER_VISIBILITY: {
			return drawerOpenPropReducer(state, action);
		}
		default: {
			return state;
		}
	}
}

function drawerOpenPropReducer(state, action) {
	return Object.assign({}, state, {open: action.payload.open});
}

export default commonDrawerReducer;