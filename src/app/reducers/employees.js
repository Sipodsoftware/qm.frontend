import {RECEIVED_EMPLOYEES, RECEIVED_EMPLOYEE} from "../actions/employees";

const initialState = {
	employees: [],
	currentEmployee: null
};

function EmployeesReducer(state = initialState, action) {
	switch (action.type) {
		case RECEIVED_EMPLOYEES: {
			return Object.assign({}, state, {employees: action.payload.employees});
		}
		case RECEIVED_EMPLOYEE: {
			return Object.assign({}, state, {currentEmployee: action.payload.currentEmployee});
		}
		default: {
			return state;
		}
	}
}

export default EmployeesReducer;