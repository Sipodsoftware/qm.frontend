import React from "react";
import {connect} from "react-redux";

import {openDrawer, closeDrawer, setDrawerVisibility} from "../../actions/common/drawer";

// Components
import AppHeader from "../../components/header/header.jsx";
import AppDrawer from "../../components/drawer/drawer.jsx";

const AppLayout = React.createClass({
	render() {
		return (
			<div>
				<AppHeader openDrawerHandler={this.openDrawerHandler} />
				<AppDrawer onRequestChangeHandler={this.setDrawerVisibility} />
				<div>
					{this.props.children}
				</div>
			</div>
		);
	},
	openDrawerHandler() {
		const {dispatch} = this.props;
		dispatch(openDrawer());
	},
	closeDrawerHandler() {
		const {dispatch} = this.props;
		dispatch(closeDrawer());
	},
	setDrawerVisibility(open) {
		const {dispatch} = this.props;
		dispatch(setDrawerVisibility(open));
	}
});

function mapStateToProps(state) {
	return {};
}

export default connect(mapStateToProps)(AppLayout);